FROM debian:9

ENV PACKER_VER=1.3.4
ENV ANSIBLE_VER=2.7

RUN \
    apt-get update && \
    apt-get install -y --no-install-recommends \
    ca-certificates \
    git \
    python3 \
    python3-pip \
    python3-setuptools \
    qemu-utils \
    qemu-system-x86 \
    qemu-kvm \
    unzip \
    wget &&\
    apt-get autoremove && \
    apt-get autoclean

RUN \
    wget https://releases.hashicorp.com/packer/${PACKER_VER}/packer_${PACKER_VER}_linux_amd64.zip &&\
    unzip packer_${PACKER_VER}_linux_amd64.zip &&\
    mv packer /usr/bin/ &&\
    rm -rf packer_${PACKER_VER}_linux_amd64.zip 

RUN \
    pip3 install ansible==${ANSIBLE_VER}

RUN groupadd --system libvirt &&\
    groupadd --gid 1000 ca && \
    useradd --no-log-init --create-home --uid 1000 --gid 1000 ca &&\
    adduser ca libvirt 

USER ca
ENV USER ca

