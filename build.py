#!/usr/bin/env python3
from sys import version_info
from argparse import ArgumentParser

if version_info <= (3, 5):
    raise Exception("Must be using Python 3.5+")
else:
    from packer import packer


def main():
    parser = ArgumentParser()
    parser.add_argument("-o", "--operating_system", type=str,
                        action='store', dest='os',  required=True,
                        help="which operating system build")
    parser.add_argument("-b", "--builder", type=str, choices=["qemu", "vbox"],
                        action='store', dest='builder', required=True,
                        help="which builder use")
    args = parser.parse_args()

    generated_json = 'generated.json'

    p = packer(args.os, args.builder)
    p.generate_json(generated_json)
    p.validate_json(generated_json)
    p.run(generated_json)


if __name__ == '__main__':
    main()
