all: alpine centos debian
clean:
	rm -rf *output *.log

vbox: alpine_vbox centos_vbox debian_vbox
qemu: alpine_qemu centos_qemu debian_qemu

alpine: alpine_vbox alpine_qemu
alpine_vbox:
	python3 build.py -o alpine -b vbox
alpine_qemu:
	python3 build.py -o alpine -b qemu

centos: centos_vbox centos_qemu
centos_vbox:
	python3 build.py -o centos -b vbox
centos_qemu:
	python3 build.py -o centos -b qemu

debian: debian_vbox debian_qemu
debian_vbox:
	python3 build.py -o debian  -b vbox
debian_qemu:
	python3 build.py -o debian -b qemu
