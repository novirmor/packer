from json import load, dump
from os import environ, path
from subprocess import run, PIPE


class packer:
    def __init__(self, os, builder):
        if not path.isfile("vars/{}.json".format(os)):
            raise Exception("file: vars/{}.json not exist".format(os))
        if not path.isfile("http/{}/{}.cfg".format(builder, os)):
            raise Exception(
                "file: http/{}/{}.cfg not exist".format(builder, os))
        self.os = os
        if builder == 'vbox':
            self.builder = 'virtualbox-iso'
        if builder == 'qemu':
            self.builder = 'qemu'

    def generate_json(self, json):
        with open('template.json', 'r') as json_data:
            template = load(json_data)
        with open("vars/{}.json".format(self.os), 'r') as json_data:
            var = load(json_data)
        template["variables"] = var["variables"]
        for builder in template["builders"]:
            if builder["type"] == self.builder:
                builder["boot_command"] = var["boot_command"]
        with open(json, 'w') as outfile:
            dump(template, outfile)

    def validate_json(self, json):
        cmd = ['packer', 'validate',  "-only={}".format(self.builder), json]
        process = run(cmd, stdout=PIPE)
        if process.returncode != 0:
            raise Exception(
                "Generated string is not valid\n packer msg:{}".format(process.stdout))

    def run(self, json):
        cmd = ['packer', 'build', "-only={}".format(self.builder), json]
        env = environ.copy()
        env["PACKER_LOG"] = "1"
        env["PACKER_LOG_PATH"] = "{}-{}-build.log".format(self.os, self.builder)
        process = run(cmd, env=env)
        if process.returncode != 0:
            raise Exception(
                "Build Failed!\n packer msg:{}".format(process.stdout))
